variables:
  FF_USE_FASTZIP: "true" # enable fastzip - a faster zip implementation that also supports level configuration.
  ARTIFACT_COMPRESSION_LEVEL: default
  CACHE_COMPRESSION_LEVEL: default
  TRANSFER_METER_FREQUENCY: 5s # will display transfer progress every 5 seconds for artifacts and remote caches.
  MAVEN_OPTS: "-Djava.awt.headless=true -Dmaven.repo.local=${CI_PROJECT_DIR}/.m2/repository/"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"
  SONAR_USER_HOME: ${CI_PROJECT_DIR}/.sonar  # Defines the location of the analysis task cache
  GIT_DEPTH: 0  # Tells git to fetch all the branches of the project, required by the analysis task
  DOCKER_BUILDKIT: 1 # With BuildKit, you don't need to pull the remote images before building since it caches each build layer in your image registry. Then, when you build the image, each layer is downloaded as needed during the build.
  ARTIFACTS_DOWNLOAD_REF: "master"

stages:          # List of stages for jobs, and their order of execution
  - dockerimage
  - deploy


#-----------------------------------------------------------------------------------------------------------------------
# Dockerimage
#-----------------------------------------------------------------------------------------------------------------------
docker-build:
  image: docker:20.10.7-git
  stage: dockerimage
  services:
    - docker:dind
  before_script:
    - ls -lR
    - mkdir ./config
    #- mv ./oKBereitschaftsplanungBackend/target/spbe.war ./spbe.war
    - mv ./oKBereitschaftsplanungBackend/target/spbe ./
    - mv ./oKBereitschaftsplanungBackend/Dockerfile ./Dockerfile
    - mv ./oKBereitschaftsplanungBackend/src/config/kubernetes/ ./config/
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - printenv
    - ls -lR
    - |
      if [[ "$CI_COMMIT_SHORT_SHA_CHILD" == "" ]]; then
        CI_COMMIT_SHORT_SHA_CHILD="${CI_COMMIT_SHORT_SHA}-downstream";
      fi
    - |
      if [[ "$CI_COMMIT_SHORT_SHA" == "" ]]; then        
        CI_COMMIT_SHORT_SHA_CHILD=$(head /dev/urandom | tr -dc 'a-f0-9' | head -c 7);
        CI_COMMIT_SHORT_SHA_CHILD="${CI_COMMIT_SHORT_SHA_CHILD}-generated";
      fi
    - |
      if [[ "$CI_COMMIT_TAG_CHILD" == "" ]]; then
        tag="sha-${CI_COMMIT_SHORT_SHA_CHILD}"
        CI_SOURCE_BRANCH="$ARTIFACTS_DOWNLOAD_REF"
      else
        tag="$CI_COMMIT_TAG_CHILD";
        CI_SOURCE_BRANCH="tag"
      fi
    - |
      if [[ "$CI_COMMIT_TAG" != "" ]]; then
        tag="$CI_COMMIT_TAG"
        CI_SOURCE_BRANCH="tag"
      fi
    - echo current ARTIFACTS_DOWNLOAD_REF $ARTIFACTS_DOWNLOAD_REF
    - echo current CI_COMMIT_TAG_CHILD $CI_COMMIT_TAG_CHILD
    - echo current CI_COMMIT_TAG $CI_COMMIT_TAG
    - echo current CI_COMMIT_SHORT_SHA_CHILD $CI_COMMIT_SHORT_SHA_CHILD
    - echo current tag ${tag}
    # - CI_SOURCE_BRANCH=$(git for-each-ref | grep $CI_COMMIT_SHORT_SHA_CHILD | grep origin | sed "s/.*\///" | tr '[:upper:]' '[:lower:]')
    - echo CI_SOURCE_BRANCH $CI_SOURCE_BRANCH
    - REGISTRY_IMAGE_BASE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/standbyplanning"
    - FINAL_REGISTRY_IMAGE="$CI_REGISTRY_IMAGE/$CI_SOURCE_BRANCH/standbyplanning:${tag}"
    - echo "FINAL_REGISTRY_IMAGE=$FINAL_REGISTRY_IMAGE" >> dockerimage.env
    - echo "REGISTRY_IMAGE_BASE=$REGISTRY_IMAGE_BASE" >> dockerimage.env
    - echo "IMAGE_TAG=$tag" >> dockerimage.env
    - echo current FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - docker build --pull -t "$FINAL_REGISTRY_IMAGE" -t "$REGISTRY_IMAGE_BASE" --cache-from "$REGISTRY_IMAGE_BASE"  --build-arg BUILDKIT_INLINE_CACHE=1 .
    #- docker build --pull -t "$FINAL_REGISTRY_IMAGE" -t "$REGISTRY_IMAGE_BASE" .
    - docker push "$REGISTRY_IMAGE_BASE" --all-tags
  needs:
    - project: openkonsequenz/qa/standbyplanning.backend
      job: build-job
      ref: $ARTIFACTS_DOWNLOAD_REF
      artifacts: true
    - project: openkonsequenz/qa/standbyplanning.frontend
      job: build-job
      ref: $ARTIFACTS_DOWNLOAD_REF
      artifacts: true
  artifacts:
    reports:
      dotenv: dockerimage.env

#-----------------------------------------------------------------------------------------------------------------------
# Deploy
#-----------------------------------------------------------------------------------------------------------------------
.deploy-script:
  image: alpine:3.14.0
  cache: {}
  variables:
    GIT_STRATEGY: none
    DEPLOYMENT_FILE: fileDefaultVarPlaceholder
    YAML_IMAGE_NAME: image
  before_script:
    - apk add --no-cache git curl bash coreutils
    - wget https://github.com/mikefarah/yq/releases/download/v4.17.2/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
    - yq -V
    - ls -l
    - git clone https://${CI_USERNAME}:${CI_PUSH_TOKEN}@gitlab.com/${GITLAB_DEPLOYMENT_REPO_URL}
    - cd *
    - git config --global user.email "gitlab@gitlab.com"
    - git config --global user.name "GitLab CI/CD"
  script:
    - ls -l
    - cat ${DEPLOYMENT_FILE}
    - echo ARTIFACTS_DOWNLOAD_REF ${ARTIFACTS_DOWNLOAD_REF}
    - echo FINAL_REGISTRY_IMAGE ${FINAL_REGISTRY_IMAGE}
    - echo APP_NAME ${APP_NAME}
    - echo IMAGE_TAG ${IMAGE_TAG}
    - echo REGISTRY_IMAGE_BASE ${REGISTRY_IMAGE_BASE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].tag = env(IMAGE_TAG)' ${DEPLOYMENT_FILE}
    - yq e -i '.apps.[env(YAML_APP_NAME)][env(YAML_IMAGE_NAME)].repository = env(REGISTRY_IMAGE_BASE)' ${DEPLOYMENT_FILE}
    - cat ${DEPLOYMENT_FILE}
    # - git commit -am '[skip ci] Image update'
    # - git push origin main
    - git diff --exit-code || git commit -am '[skip ci] Image update' && git push origin main


#------------------------------
# Deploy - QA-Environment
#------------------------------
deploy-qa-standbyplanning:
  stage: deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: standbyplanning
    DEPLOYMENT_FILE: deployment/applications/values-standbyplanning-qa.yaml
  needs:
    - job: docker-build
  dependencies:
    - docker-build
  only:
    variables:
      - $ARTIFACTS_DOWNLOAD_REF == "main" || $ARTIFACTS_DOWNLOAD_REF == "master" || $CI_COMMIT_TAG_CHILD != ""
  #|| $CI_COMMIT_TAG != ""
  # rules:
  #   - if: '$CI_COMMIT_TAG != ""'
  #   - if: '$ARTIFACTS_DOWNLOAD_REF = "master"'
  #   - if: '$ARTIFACTS_DOWNLOAD_REF = "main"'
  # only:
  #   - main
  #   - tags


#------------------------------
# Deploy - DEV-Environment
#------------------------------
deploy-dev-standbyplanning:
  stage: deploy
  extends: .deploy-script
  variables:
    YAML_APP_NAME: standbyplanning
    DEPLOYMENT_FILE: deployment/applications/values-standbyplanning-dev.yaml
  needs:
    - job: docker-build
  dependencies:
    - docker-build
  # rules:
  #   - if: '$ARTIFACTS_DOWNLOAD_REF == "develop"'
  only:
    variables:
      - $ARTIFACTS_DOWNLOAD_REF == "develop"
  # only:
  #   - develop
